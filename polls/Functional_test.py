_author_  = 'dh.mahecha, jl.pulgarin'
from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

class FunctionalTest(TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()


    def tearDown(self):
        self.browser.quit()

    def test_title(self):
         self.browser.get("http://localhost:8000")
         self.assertIn('Busco Ayuda', self.browser.title)

    def test_registro(self):
        self.browser.get("http://localhost:8000")

        link = self.browser.find_element_by_id('id_register')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Pedro')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Picapiedra')

        aniosExperiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        aniosExperiencia.send_keys('3')

        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('21111111')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('pedropicapiedra@gmail.com')

        imagen = self.browser.find_element_by_id('id_imagen')
        imagen.send_keys('C:\programas\Fredflintstone.jpg')

        username = self.browser.find_element_by_id('id_username')
        username.send_keys('p.picapiedra')

        password = self.browser.find_element_by_id('id_password')
        password.send_keys('password')

        self.browser.find_element_by_xpath("//select[@id='id_tiposDeServicio']/option[text()='Desarrollador Web']").click()

        link = self.browser.find_element_by_id('id_grabar')
        link.click()

        self.browser.implicitly_wait(50)


        span = self.browser.find_element(By.XPATH,'//span[text()="Pedro Picapiedra"]')

        self.assertIn('Pedro Picapiedra', span.text)

    def test_verDetalle(self):
        self.browser.get("http://localhost:8000")
        span = self.browser.find_element(By.XPATH, '//span[text()="Pedro Picapiedra"]')
        span.click()

        h2=self.browser.find_element(By.XPATH, '//h2[text()="Pedro Picapiedra"]')

        self.assertIn('Pedro Picapiedra',h2.text)


    def test_login(self):
        self.browser.get('http://localhost:8000')

        link = self.browser.find_element_by_id('id_login')
        link.click()


        self.browser.implicitly_wait(10)
        modal = WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((By.ID, "login_modal")))

        usuario = modal.find_element(By.NAME, "username")
        usuario.send_keys('p.picapiedra')

        password = modal.find_element(By.NAME, "password")
        password.send_keys('password')

        link = self.browser.find_element_by_id('id_entrar')
        link.click()

        boton_logout = self.browser.find_element_by_id('id_logout')
        self.assertNotEqual(boton_logout, None)


    def test_editar(self):
        self.browser.get('http://localhost:8000')

        link = self.browser.find_element_by_id('id_login')
        link.click()

        self.browser.implicitly_wait(10)
        modal = WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((By.ID, "login_modal")))

        usuario = modal.find_element(By.NAME, "username")
        usuario.send_keys('p.picapiedra')

        password = modal.find_element(By.NAME, "password")
        password.send_keys('password')

        link = self.browser.find_element_by_id('id_entrar')
        link.click()

        link = self.browser.find_element_by_id('id_editar')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.clear()
        nombre.send_keys('Pablo')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.clear()
        apellidos.send_keys('Marmol')

        aniosExperiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        aniosExperiencia.clear()
        aniosExperiencia.send_keys('3')

        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.clear()
        telefono.send_keys('21111111')

        link = self.browser.find_element_by_id('id_grabar')
        link.click()

        h2=self.browser.find_element(By.XPATH, '//h2[text()="Pablo Marmol"]')


        boton_logout = self.browser.find_element_by_id('id_logout')
        self.assertIn('Pablo Marmol',h2.text)


    def test_registrar_comentarios(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('trabajador_6')
        link.click()

        nombre = self.browser.find_element_by_id('correo')
        nombre.send_keys('vilma_picapiedra@gmail.com')

        nombre = self.browser.find_element_by_id('comentario')
        nombre.send_keys('Este es un comentario del sitio')


        self.browser.implicitly_wait(10)

        link = self.browser.find_element_by_id('comentar')
        link.click()

        h4 = self.browser.find_element(By.XPATH, '//h4[text()="vilma_picapiedra@gmail.com"]')

        self.assertIn('vilma_picapiedra@gmail.com', h4.text)

